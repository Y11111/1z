package com.sycu.socket.unit07;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServer {
    public static void main(String[] args) {
        final int DEFAULT_PORT = 8888;
        ServerSocket serverSocket = null;
        Socket socket = null;
        BufferedReader reader = null;
        BufferedWriter writer = null;
        try {
            // 绑定监听端口
            serverSocket = new ServerSocket(DEFAULT_PORT);
            System.out.println("启动服务器，监听端口 " + DEFAULT_PORT);
            while (true) {
                // 等待客户端连接
                socket = serverSocket.accept();

                reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

                String msg = null;
                // 读取客户端发送的消息，并且进行回复
                while ((msg = reader.readLine())!= null) {
                    // 如果消息表示上传文件开始
                    if (msg.equals("START_UPLOAD")) {
                        // 创建文件写入器，将接收到的内容写入 B.html
                        BufferedWriter fileWriter = new BufferedWriter(new FileWriter("B.html"));
                        String line;
                        while ((line = reader.readLine())!= null &&!line.equals("END_UPLOAD")) {
                            fileWriter.write(line + "\n");
                        }
                        fileWriter.close();
                        writer.write("服务器：文件上传成功\n");
                        writer.flush();
                        System.out.println("文件上传成功");
                    } else if (msg.equals("START_DOWNLOAD")) {
                        // 读取 C.html 文件内容并发送给客户端
                        BufferedReader fileReader = new BufferedReader(new FileReader("C.html"));
                        String content;
                        writer.write("SERVER_SENDING_FILE\n");
                        writer.flush();
                        while ((content = fileReader.readLine())!= null) {
                            writer.write(content + "\n");
                            writer.flush();
                        }
                        writer.write("END_DOWNLOAD\n");
                        writer.flush();
                        fileReader.close();
                        System.out.println("文件下载成功");
                    } else {
                        // 回复客户端
                        writer.write("服务器：已收到 - " + msg + "\n");
                        writer.flush();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (serverSocket!= null) {
                try {
                    serverSocket.close();
                    reader.close();
                    writer.close();
                    System.out.println("关闭 serverSocket");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}