package com.sycu.socket.unit07;
import java.io.FileOutputStream;  
import java.io.IOException;  
import java.net.DatagramPacket;  
import java.net.DatagramSocket;  
import java.net.InetAddress;  
  
public class UDPReceiver {  
    public static void main(String[] args) {  
        final int SERVER_PORT = 10086;  
        final String DESTINATION_PATH = "D:\\b\\3.html";  
        DatagramSocket receiver = null;  
        FileOutputStream fos = null;  
  
        try {  
            // 创建UDP套接字并绑定到指定端口  
            receiver = new DatagramSocket(SERVER_PORT);  
            System.out.println("接收端已启动，等待接收文件...");  
  
            byte[] buffer = new byte[1024];  
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);  
  
            // 用于存储文件数据的字节数组  
            ByteArrayOutputStream baos = new ByteArrayOutputStream();  
  
            while (true) {  
                // 接收数据报  
                receiver.receive(packet);  
  
                // 将接收到的数据添加到baos中  
                baos.write(packet.getData(), 0, packet.getLength());  
  
                // 检查是否是文件结束信号（例如，空数据包）  
                if (packet.getLength() == 0) {  
                    break;  
                }  
            }  
  
            // 将baos中的数据写入文件  
            byte[] fileData = baos.toByteArray();  
            fos = new FileOutputStream(DESTINATION_PATH);  
            fos.write(fileData);  
  
            // 显示文件内容  
            System.out.println("文件已接收并保存到 " + DESTINATION_PATH);  
            // 注意：这里为了简单起见，不直接显示docx内容，而是提示用户查看文件  
            // 真实场景中，需要专门的库来解析和显示docx内容  
  
        } catch (Exception e) {  
            e.printStackTrace();  
        } finally {  
            try {  
                if (fos != null) fos.close();  
                if (receiver != null) receiver.close();  
            } catch (IOException e) {  
                e.printStackTrace();  
            }  
        }  
    }  
}